//
//  light_iosApp.swift
//  light_ios
//
//  Created by Szymon Mańka on 23/11/2021.
//

import SwiftUI

@main
struct light_iosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }

}
