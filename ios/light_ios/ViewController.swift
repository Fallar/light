import UIKit
import AppTrackingTransparency
import AdSupport

class ViewController: UIViewController, UITableViewDelegate {
  
  // MARK: - Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    requestPermission()
  }
  
  // MARK: - UI Settings
  override var shouldAutorotate : Bool {
    return false
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return UIStatusBarStyle.lightContent
  }
  
  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }
    
    func requestPermission() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    print("Authorized")
                    let adId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
                    if let storedId = KeyChain.load(key: adId) {
                        let result = storedId.to(type: String.self)
                        print("result: ", result)
                    } else {
                        let uuid = UUID().uuidString
                        let data = Data(from: uuid)
                        let status = KeyChain.save(key: adId, data: data)
                        print("status: ", status)
                    }
                    print()
                case .denied:
                    print("Denied")
                case .notDetermined:
                    print("Not Determined")
                case .restricted:
                    print("Restricted")
                @unknown default:
                    print("Unknown")
                }
            }
        }
    }
  
}
