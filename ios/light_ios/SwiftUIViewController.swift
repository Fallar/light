//
//  SwiftUIViewController.swift
//  light_ios
//
//  Created by Szymon Mańka on 23/11/2021.
//

import SwiftUI

struct SwiftUIViewController: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SwiftUIViewController_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIViewController()
    }
}
