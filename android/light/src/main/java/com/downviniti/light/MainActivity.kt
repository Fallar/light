package com.downviniti.light

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.downviniti.light.api.ApiInterface
import com.downviniti.light.api.ModelRequest
import com.downviniti.light.databinding.ActivityMainBinding
import com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo
import io.reactivex.rxjava3.core.Observable.fromCallable
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var shared: SharedPreferences
    private lateinit var generatedUUIDText: TextView
    private lateinit var readUUIDText: TextView
    private lateinit var generatedUUIDButton: Button
    private lateinit var readUUIDButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        shared = getSharedPreferences(getString(R.string.shared_name), Context.MODE_PRIVATE)
        generatedUUIDText = findViewById(R.id.generated_uuid_text)
        readUUIDText = findViewById(R.id.read_uuid_text)
        generatedUUIDButton = findViewById(R.id.generate_uuid_button)
        readUUIDButton = findViewById(R.id.read_uuid_button)

        generatedUUIDButton.setOnClickListener {
            val uuid: UUID = UUID.randomUUID()
            saveInPrefs(uuid.toString())
            generatedUUIDText.text = uuid.toString()
        }

        readUUIDButton.setOnClickListener {
            val token = readPrefs()
            readUUIDText.text = token
        }
    }

    private fun saveInPrefs(uuid: String) {
        with(shared.edit()) {
            putString(getString(R.string.shared_ID_key), uuid)
            apply()
        }
    }

    private fun readPrefs(): String {
        return shared.getString(getString(R.string.shared_ID_key), "NIE MA") ?: "NIE MA"
    }
}